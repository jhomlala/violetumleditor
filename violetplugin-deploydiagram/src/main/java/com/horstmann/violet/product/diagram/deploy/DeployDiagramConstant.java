package com.horstmann.violet.product.diagram.deploy;

public interface DeployDiagramConstant
{

    public static final String DEPLOY_DIAGRAM_STRINGS = "properties.DeployDiagramGraphStrings";
      
}
